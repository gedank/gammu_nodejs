var request = require('request');
var fs = require("fs");
var exec = require('child_process').exec;

exports.create = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _command = "gammu-smsd -c " +_file+ " -i -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[create] The file exists!");
		}else {
			createFile(_path,'wx',_command,params.device);
			console.log("[create] The file not exists!");
		}
	});
};

exports.start = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _command = "gammu-smsd -c " +_file+ " -s -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[start] The file exists!");
		}else {
			console.log("[start] The file not exists!");
		}
	});
};

exports.run = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _command = "gammu-smsd -c " +_file+ " -S -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[run] The file exists!");
		}else {
			console.log("[run] The file not exists!");
		}
	});
};

exports.stop = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _command = "gammu-smsd -c " +_file+ " -k -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[stop] The file exists!");
		}else {
			console.log("[stop] The file not exists!");
		}
	});
};

exports.delete = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	// _path.remove();
	var _command = "gammu-smsd -c " +_file+ " -u -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[delete] The file exists!");
		}else {
			console.log("[delete] The file not exists!");
		}
	});
};

exports.cekDevice = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _index = params.device.db_index-1;
	var _command = "gammu -s " +_index+ " -c gammurc identify";
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command, puts);
			console.log("[cekDevice] The file exists!");
		}else {
			console.log("[cekDevice] The file not exists!");
		}
	});
};

exports.cekPulsa = function(params){
	var _file = 'smsdrc_' + params.device.db_index;
	var _path = params.config.path + _file;
	var _index = params.device.db_index-1;
	var _command1 = "gammu-smsd -c " +_file+ " -k -n gammuSMSD-" +params.device.db_index;
	var _command2 = "gammu -s " +_index+ " -c -n gammurc getussd gammuSMSD-" +params.device.db_ussd;
	var _command3 = "gammu-smsd -c " +_file+ " -s -n gammuSMSD-" +params.device.db_index;
	fs.exists(_path, function(exists) {
		if(exists) {
			exec(_command1, puts);
			exec(_command2, puts);
			exec(_command3, puts);
			console.log("[cekPulsa] The file exists!");
		}else {
			console.log("[cekPulsa] The file not exists!");
		}
	});
};


exports.apiCreate = function(params){
	console.log('apiCreate');
	request.post({
        url: params.config.apiurl+'?mode=create',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiStart = function(params){
	console.log('apiStart');
	request.post({
        url: params.config.apiurl+'?mode=start',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiRun = function(params){
	console.log('apiRun');
	request.post({
        url: params.config.apiurl+'?mode=run',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiStop = function(params){
	console.log('apiStop');
	request.post({
        url: params.config.apiurl+'?mode=stop',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiDelete = function(params){
	console.log('apiDelete');
	request.post({
        url: params.config.apiurl+'?mode=delete',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiCekDevice = function(params){
	console.log('apiCekDevice');
	request.post({
        url: params.config.apiurl+'?mode=cekDevice',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}

exports.apiCekPulsa = function(params){
	console.log('apiCekPulsa');
	request.post({
        url: params.config.apiurl+'?mode=cekPulsa',
        body: "phoneId="+params.device.phones_id
    },
    function(error, response, body){
    	console.log(body);
    });
}


// create file
function createFile(path, mode, command, device) {
	var id         = device.ID;
	var smsdrc     = 'smsdrc_'+device.db_index;
	var port       = device.Port;
	var connection = device.Connection;
	var send       = device.Send;
	var receive    = device.Receive;
	var content = "[gammu]\n"
		+"port = "+port+"\n"
		+"connection = "+connection+"\n"
		+"\n"
		+"[smsd]\n"
		+"service = mysql\n"
		+"logfile = log"+smsdrc+"\n"
		+"debuglevel = 0\n"
		+"phoneid = "+id+"\n"
		+"commtimeout = 300\n"
		+"sendtimeout = 300\n"
		+"send = "+send+"\n"
		+"receive = "+receive+"\n"
		+"checksecurity = 0\n"
		+"#PIN = 1234\n"
		+"\n"
		+"pc = "+device.db_Host+"\n"
		+"user = "+device.db_User+"\n"
		+"password = "+device.db_Pass+"\n"
		+"database = "+device.db_Name+"\n";
	fs.open(path, mode, function(err, fd){
		if(err)
			console.log("The file can't open!");
		else
			fs.writeFile(path, content, function(err) {
				if(err) {
					console.log("The file can't write!");
				}
				else  {
					console.log("The file was saved!");
				}
			});
	});
}

function readFile(path, command) {
	fs.readFile(path, 'utf8', function (err,data) {
		if (err) {
			console.log(err);
		} else {
			exec(_command, puts);
		}
	});
}
// create file

// exec command 
function puts(error, stdout, stderr) {
	console.log(stdout);
}
// exec command