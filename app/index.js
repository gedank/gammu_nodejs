var express = require('express');
var app = express();
var request = require('request');
var server = require('http').Server(app);
var bodyParser = require("body-parser");

var gammu = require('./controller/gammuController');
var conf = require('./config/config');
var config = conf.config;

/* get url */
var interval = 20000;
var autoload = setTimeout(function exec() {
	var today = new Date();
	var curr_year = today.getFullYear();
	var curr_month = today.getMonth();
	var curr_day = today.getDay();
    var curr_hour = today.getHours();
    var curr_min = today.getMinutes();
    var curr_sec = today.getSeconds();
    var date = curr_year+'-'+curr_month+'-'+curr_day;
    var time = curr_hour+'-'+curr_min+':'+curr_sec;
    console.log(date+' '+time+' - running...');

    request(config.apiurl + '?mode=device', function (error, response, html) {
		if (!error && response.statusCode == 200) {
			var obj = JSON.parse(html);
			if(obj.data == null) {
				console.log('log: ' + obj.msg);
			} else {
			    var hour = (curr_hour < 10) ? '0' + curr_hour : curr_hour;
			    var strtotime = (curr_hour * 3600) + (curr_min * 60) + curr_sec;
			    var range = 15 * 60;
				var data = obj.data;
				data.forEach(function(device) {
					if(strtotime > range) {
			    		if(device.sts_create == '1'){
			    			var params = {
			    				config: config,
			    				device: device
			    			};
			    			if(gammu.cekDevice(params) != null){
			    				gammu.apiCekDevice(params);
			    			}
			    			if(gammu.create(params) != null){
			    				gammu.apiCreate(params);
			    			}
			    		}

			    		if(device.sts_start == '1'){
			    			var params = {
			    				config: config,
			    				device: device
			    			};
			    			if(gammu.start(params) != null){
			    				gammu.apiStart(params);
			    			}
			    		}

			    		if(device.sts_run == '1'){
			    			var params = {
			    				config: config,
			    				device: device
			    			};
			    			if(gammu.run(params) != null){
			    				gammu.apiRun(params);
			    			}
			    		}

			    		if(device.sts_stop == '1'){
			    			var params = {
			    				config: config,
			    				device: device
			    			};
			    			if(gammu.stop(params) != null){
			    				gammu.apiStop(params);
			    			}
			    		}

			    		if(device.sts_delete == '1'){
			    			var params = {
			    				config: config,
			    				device: device
			    			};
			    			if(gammu.delete(params) != null){
			    				gammu.apiDelete(params);
			    			}
			    		}
			    	}

			    	if(curr_hour+':'+curr_min+':'+curr_sec == '00:00:00') {
			    		var params = {
							config: config,
							device: device
						};
			    		if(gammu.cekPulsa(params) != null){
			    			gammu.apiCekPulsa(params);
			    		}
			    	}
				});
			}
		}
	});
    autoload = setTimeout(exec, interval);
}, interval/interval);
/**/

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

server.listen(5000);

app.use('/', function(req, res){
	res.json({'msg':'index'});
});